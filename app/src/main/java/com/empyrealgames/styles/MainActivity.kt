package com.empyrealgames.styles

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if( getSharedPreferences("THEME", Context.MODE_PRIVATE).getString("CURR", "LIGHT")=="LIGHT"){
            println("onCreate found LIGHT")
            setTheme(R.style.AppLightTheme)
        }else{
            println("onCreate found DARK")
            setTheme(R.style.AppDarkTheme)
         }
        setContentView(R.layout.activity_main)
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    fun changeState(view: View){
        button1.isEnabled = !(button1.isEnabled)
    }

    fun changeTheme(view: View){
       if( getSharedPreferences("THEME", Context.MODE_PRIVATE).getString("CURR", "LIGHT") == "LIGHT"){
           println("button found LIGHT")
          getSharedPreferences("THEME",Context.MODE_PRIVATE).edit().putString("CURR","DARK").apply()
       }else{
           getSharedPreferences("THEME",Context.MODE_PRIVATE).edit().putString("CURR","LIGHT").apply()
           println("button found DARK")
       }

        startActivity(Intent(this, MainActivity::class.java))
    }

}
