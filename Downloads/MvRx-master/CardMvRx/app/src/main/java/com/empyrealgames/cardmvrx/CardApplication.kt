package com.empyrealgames.cardmvrx

import android.app.Application


class CardApplication : Application() {

    private val apiService: CardRetrofit = CardRetrofit.create()
    val cardRepository =  CardRepository(apiService)

}