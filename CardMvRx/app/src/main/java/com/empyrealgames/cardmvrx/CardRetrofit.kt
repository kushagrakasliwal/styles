package com.empyrealgames.cardmvrx

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface CardRetrofit {
    @GET("posts")
    fun getCards() : Observable<List<Card>>

    companion object Factory {
        fun create(): CardRetrofit {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .build()
            return retrofit.create(CardRetrofit::class.java)
        }
    }
}